<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Set Operations for Arrays

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)
*  [Examples](#examples)
*  [Additional Information](#additional-information)

## Overview

This JST allows IAP users to do set operations on 2 user provided arrays. The JST removes duplicates from both of the input arrays before processing them and uses the [Remove Duplicates from Array Of Objects Or Arrays](https://gitlab.com/itentialopensource/pre-built-automations/remove-duplicates-from-array-of-objects-or-arrays) pre-built for that purpose. Arrays with deeply nested objects are also supported. 
Please note, the worst case in time complexity for this JST is $`O(n^2)`$ hence the operation might be slow for sufficiently large datasets.

## Installation Prerequisites
Users must satisfy the following prerequisites:
* Itential Automation Platform: `^2022.1.x`

## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.

## How to Run

Use the following to run the pre-built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the section in your workflow where you would like to convert a string to an object and add a `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `setOperationsForArrays` (the name of the internal JST).

3. The inputs to the JST would be the 2 arrays that you want to do the set operations on.

4. Save your input and the task is ready to run inside of IAP.

## Attributes  

Attributes for the pre-built are outlined in the following tables.

1. Input:
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>array1</code></td>
<td>first operand for the set operation</td>
<td><code>array[strings/numbers/integers/objects/arrays]</code></td>
</tr>
<tr>
<td><code>array2</code></td>
<td>second operand for the set operation</td>
<td><code>array[strings/numbers/integers/objects/arrays]</code></td>
</tr>
</tbody>
</table>


2. Output:
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>intersection</code></td>
<td>common values in both the arrays</td>
<td><code>array</code></td>
</tr>
<tr>
<td><code>array1-array2</code></td>
<td>values in array1 that don't exist in array 2</td>
<td><code>array</code></td>
</tr>
<tr>
<td><code>array2-array1</code></td>
<td>values in array2 that don't exist in array 1</td>
<td><code>array</code></td>
</tr>
<tr>
<td><code>difference</code></td>
<td>combination of values in array1 and array 2 without the common elements</td>
<td><code>array</code></td>
</tr>
<tr>
<td><code>union</code></td>
<td>combination of values in array1 and array 2 with one instance of the common elements</td>
<td><code>array</code></td>
</tr>
</tbody>
</table>

## Examples
Examples describing how the pre-built will work for different inputs.

**Example 1** 

Input: 
```
{
  "array1": [1,2,3,4,5],
  "array2": [4,5,6,7,8]
}
  ```

Output:
```
{
  "union": [1,2,3,4,5,6,7,8],
  "array2-array1": [6,7,8],
  "array1-array2": [1,2,3],
  "intersection": [4,5],
  "difference": [1,2,3,6,7,8]
}
  ```
   <hr><br>
    
**Example 2**  

Input: 
```
{
  "array1": ["pineapple","banana","apple","grapes"],
  "array2": ["pineapple","banana","mango","orange"]
}
  ```

Output:
```
{
  "union": ["pineapple","banana","apple","grapes","mango","orange"],
  "array2-array1": ["mango","orange"],
  "array1-array2": ["apple","grapes"],
  "intersection": ["pineapple","banana"],
  "difference": ["apple","grapes","mango","orange"]
}
  ```
  <hr><br>
  
**Example 3** 

Input: 
```
{
  "array1": [
              [1,3,4],[1,3,4],[1,2],"apple"
            ],
  "array2": [
              [1,3,4],[1,3,5],"apple",99
            ]
}
  ```

Output:
```
{
  "union": [
            [1,3,4],[1,2],"apple",[1,3,5],99
           ],
  "array2-array1": [
                    [1,3,5], 99
                   ],
  "array1-array2": [
                    [1,2]
                   ],
  "intersection": [
                    [1,3,4],"apple"
                  ],
  "difference": [
                  [1,2],[1,3,5],99
                ]
}
  ```
  <hr><br>
  

**Example 4** 

Input: 
```
{
  "array1": [
    {"1": "1"},
    {"2": "2"},
    {"3": "3"}
  ],
  "array2": [
    {"1": 1},
    {"2": "2"},
    {"4": 4}
  ]
}
  ```

Output:
```
{
"union": [
    {"1": "1"},
    {"2": "2"},
    {"3": "3"},
    {"1": 1},
    {"4": 4}
  ],
  "array2-array1": [
    {"1": 1},
    {"4": 4}
  ],
  "array1-array2": [
    {"1": "1"},
    {"3": "3"}
  ],
  "intersection": [
    {"2": "2"}
  ],
  "difference": [
    {"1": "1"},
    {"3": "3"},
    {"1": 1},
    {"4": 4}
  ]
}
  ```


## Additional Information
Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
