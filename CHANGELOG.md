
## 0.0.7 [02-04-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/set-operations-for-arrays!8

---

## 0.0.6 [06-14-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/set-operations-for-arrays!4

---

## 0.0.5 [12-03-2021]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/set-operations-for-arrays!3

---

## 0.0.4 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/set-operations-for-arrays!2

---

## 0.0.3 [05-14-2021]

* Update Readme.md

See merge request itentialopensource/pre-built-automations/staging/set-operations-for-arrays!1

---

## 0.0.2 [12-23-2020]

* Update Readme.md

See merge request itentialopensource/pre-built-automations/staging/set-operations-for-arrays!1

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n
